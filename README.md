# RxJs Orm

[![pipeline status](https://gitlab.com/rxjs-orm/rxjs-orm/badges/master/pipeline.svg)](https://gitlab.com/rxjs-orm/rxjs-orm/-/commits/master)
[![coverage report](https://gitlab.com/rxjs-orm/rxjs-orm/badges/master/coverage.svg)](https://gitlab.com/rxjs-orm/rxjs-orm/-/commits/master)

Fully typed, RxJs based, ORM for Web Applications

